@extends('layout.layout')

@section('title')
	Edit Parser
@endsection

@section('content')
<h2>EDIT {{ $id->tanya }}</h2>
<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" action="{{ route('update',['id' => $id->id]) }}" method="post">
			{{ csrf_field() }}
			@if(session('after_update'))
				<div>
					{{ session('after_update') }}
				</div>
			@endif
			<table class="table table-striped">
				<tr>
					<td>Tanya</td>
					<td><input type="text" name="tanya" class="form-control" value="{{ $id->tanya }}"></td>
				</tr>
				<tr>
					<td>Isi</td>
					<td><textarea name="isi" class="form-control" rows="8" required>{{ $id->jawab }}</textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="submit" class="btn btn-primary" value="Simpan"><a href="{{ url('parser/db') }}" class="btn btn-warning">Kembali</a></td>
				</tr>
			</table>
		</form>
	</div>
</div>
@endsection