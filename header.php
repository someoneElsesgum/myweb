<!-- 
	Wildan Nugraha's
	Created by : Insyaallah, Wildan Nugraha, ST
	Finished : June, 17 2017.
	Template : Bootstrap
-->

<!DOCTYPE html>
<html>
<head>
	<?php 
	error_reporting(0);
	session_start();
	include 'cek.php';
	include 'config.php';
	?>
	<meta http-equiv="refresh" content="600">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="ldk, ldk stmik dci, ldk stmik dci absen, absen online ldk stmik dci">
    <meta name="description" content="website yang dibuat untuk absen online ldk stmik dci">
    <meta name="author" content="LDK STMIK DCI by Wildan Nugraha">
	<title>ABSEN LDK STMIK DCI</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../assets/fonts/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui/jquery-ui.css">
	<script type="text/javascript" src="../assets/js/jquery.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.js"></script>
	<script type="text/javascript" src="../assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="../assets/js/jquery-ui/jquery-ui.js"></script>	
</head>
<body>
	<div class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="http://ldk.stmik-dci.ac.id" target="_blank" class="navbar-brand">LDK STMIK DCI</a>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">				
				<ul class="nav navbar-nav navbar-right">
					<li><a id="pesan_sedia" href="#" data-toggle="modal" data-target="#modalpesan"><span class='glyphicon glyphicon-comment'></span> Pesan <?php
					$periksa=mysql_query("select absen.id,anggota.nim,anggota.nama,absen.kehadiran,absen.keterangan,absen.tgl_absen from absen INNER JOIN anggota ON anggota.nim=absen.nim where kehadiran<>'V'");
					$count = mysql_num_rows($periksa);
						print '('.$count.')';
					
					?>
					</a>
					</li>
					<li><a class="dropdown-toggle" data-toggle="dropdown" role="button" href="#">Hy , <?php echo $_SESSION['uname']  ?>&nbsp&nbsp<span class="glyphicon glyphicon-user"></span></a></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- modal input -->
	<div id="modalpesan" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Pesan Notification</h4>
				</div>
				<div class="modal-body">
					<?php 
					$periksa=mysql_query("select absen.id,anggota.nim,anggota.nama,absen.kehadiran,absen.keterangan,absen.tgl_absen from absen INNER JOIN anggota ON anggota.nim=absen.nim");
					while($q=mysql_fetch_array($periksa)){	
					if($_SESSION[level] == 'admin' or $_SESSION[level] =='sekretaris') {
						if($q['kehadiran']<>'V'){
							
							echo "<div style='padding:5px' class='alert alert-warning'><span class='glyphicon glyphicon-info-sign'></span> Kehadiran  <a style='color:red'>". $q['nama']."</a> belum di konfirmasi . silahkan konfirmasi !!</div>";
						}
					} 			
					}
						
					?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>						
				</div>				
			</div>
		</div>
	</div>

	<div class="col-md-2">
		<div class="row">
			<?php 
			/*$use=$_SESSION['uname'];
			$fo=mysql_query("select foto from admin where uname='$use'");
			while($f=mysql_fetch_array($fo)){*/
				?>				

				<div class="col-xs-6 col-md-12">
					<a class="thumbnail">
						<!--<img class="img-responsive" src="foto/<?php echo $f['foto']; ?>">-->
						<!--<img class="img-responsive" src="../logo/ldk.jpg">-->
					</a>
				</div>
				<?php 
			//}
			?>		
		</div>

		<div class="row"></div>
		<ul class="nav nav-pills nav-stacked">
			<li class="active"><a href="index.php"><span class="glyphicon glyphicon-home"></span>  Dashboard</a></li>
		<?php
		if($_SESSION['level'] == 'admin') {
		?>			
			<li><a href="anggota.php"><span class="glyphicon glyphicon-briefcase"></span>  Data Anggota</a></li>
			<li><a href="user.php"><span class="glyphicon glyphicon-user"></span>  Data User</a></li>
			<li><a href="absen_admin.php"><span class="glyphicon glyphicon-file"></span>  Daftar Absen</a></li>
			<li><a href="rekap_absen.php"><span class="glyphicon glyphicon-book"></span>  Rekap Absen</a></li>
			<li><a href="ganti_foto.php"><span class="glyphicon glyphicon-picture"></span>  Ganti Foto</a></li>
			<li><a href="ganti_pass.php"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="pemilu.php"><span class="glyphicon glyphicon-hand-up"></span>  Pemilu LDK</a></li>
		<?php
		}
		if($_SESSION['level'] == 'ketua') {
		?>
			<li><a href="anggota.php"><span class="glyphicon glyphicon-briefcase"></span>  Data Anggota</a></li>
			<li><a href="user.php"><span class="glyphicon glyphicon-user"></span>  Data User</a></li>
			<li><a href="absen_admin.php"><span class="glyphicon glyphicon-file"></span>  Daftar Absen</a></li>
			<li><a href="rekap_absen.php"><span class="glyphicon glyphicon-book"></span>  Rekap Absen</a></li>
			<li><a href="absen_anggota.php"><span class="glyphicon glyphicon-bullhorn"></span> Absen!!</a></li>
			<li><a href="ganti_pass.php"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="pemilu.php"><span class="glyphicon glyphicon-hand-up"></span>  Pemilu LDK</a></li>
		<?php
		}
		if($_SESSION['level'] == 'sekretaris') {	
		?>
			<li><a href="anggota.php"><span class="glyphicon glyphicon-briefcase"></span>  Data Anggota</a></li>
			<li><a href="user.php"><span class="glyphicon glyphicon-user"></span>  Data User</a></li>
			<li><a href="absen_admin.php"><span class="glyphicon glyphicon-file"></span>  Daftar Absen</a></li>  
			<li><a href="absen_anggota.php"><span class="glyphicon glyphicon-bullhorn"></span> Absen!!</a></li>	
			<li><a href="ganti_pass.php"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="pemilu.php"><span class="glyphicon glyphicon-hand-up"></span>  Pemilu LDK</a></li>
		<?php
		}
		if($_SESSION['level'] == 'anggota') {
		?>
			<li><a href="absen_anggota.php"><span class="glyphicon glyphicon-bullhorn"></span> Absen!!</a></li>
			<li><a href="ganti_pass.php"><span class="glyphicon glyphicon-lock"></span> Ganti Password</a></li>
			<li><a href="pemilu.php"><span class="glyphicon glyphicon-hand-up"></span>  Pemilu LDK</a></li>
		<?php
		}
		?>			
			<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span>  Logout</a></li>
		</ul>
	</div>
	<div class="col-md-10">