<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#s1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ url('/') }}">My Web</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="s1">
			<ul class="nav navbar-nav">
				<li><a href="{{ url('/') }}"><span class="fa fa-home"></span> Beranda <span class="sr-only">(current)</span></a></li>
				<!-- <li><a href="{{ route('wildan') }}"><span class="fa fa-user"></span> Profil </a></li>
				<li><a href="{{ url('/artikel') }}"><span class="fa fa-file"></span> Artikel </a></li> -->
				<li><a href="{{ url('parser/db') }}"><span class="fa fa-file"></span> Parser </a></li>
				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-book"></span> Dropdown <span class="caret"></span> </a>
					<ul class="dropdown-menu">
						<li><a href="{{ url('artikel/v_artikel') }}">Tampil Data</a></li>
						<li><a href="{{ url('artikel/tambah') }}">Input Data</a></li>
						<li><a href="#">Drop 3</a></li>
					</ul>
				</li> -->
				<!--<li><a href="?page=bukutamu">Bukutamu </a></li>-->
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>