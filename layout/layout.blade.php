<html>
	<head>
		<title>@yield('title') - Website ku</title>
		<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	</head>
	<body>
		@include('layout.navbar')
		<div class='container'>
			@yield('content')
			<hr/>
		</div>
		<footer class="text-center">
			Copyright 2018
		</footer>

    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	</body>
</html>